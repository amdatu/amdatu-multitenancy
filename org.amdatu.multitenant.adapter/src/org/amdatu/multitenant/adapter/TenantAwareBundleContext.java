/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import static org.amdatu.multitenant.Constants.PID_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BUNDLE_SCOPE_FRAMEWORK;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BUNDLE_SCOPE_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BUNDLE_SCOPE_TENANT;
import static org.amdatu.multitenant.adapter.Utils.getBindingFilter;
import static org.amdatu.multitenant.adapter.Utils.isMultiTenantAwareBundle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.multitenant.Tenant;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Wrapper class for the bundle context interface to make it tenant aware.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantAwareBundleContext implements BundleContext {

    /**
     * Provides a tenant-aware framework event.
     */
    private static class ScopedFrameworkEvent extends FrameworkEvent {

        private static final long serialVersionUID = 1L;

        public ScopedFrameworkEvent(FrameworkEvent event) {
            super(event.getType(), event.getBundle(), event.getThrowable());
        }

        @Override
        public Bundle getBundle() {
            return new TenantAwareBundle(super.getBundle());
        }
    }

    /**
     * Provides a tenant-aware service registration.
     */
    private static class ServiceRegistrationAdapter<T> implements ServiceRegistration<T> {

        private final TenantAwareBundleContext m_context;
        private final ServiceRegistration<T> m_registration;

        public ServiceRegistrationAdapter(TenantAwareBundleContext context, ServiceRegistration<T> registration) {
            m_context = context;
            m_registration = registration;
        }

        @Override
        public ServiceReference<T> getReference() {
            return m_registration.getReference();
        }

        @Override
        public void setProperties(Dictionary<String, ?> properties) {
            m_registration.setProperties(m_context.getScopedProperties(m_registration, properties));
        }

        @Override
        public void unregister() {
            m_context.unregisterService(this);
        }

        /**
         * @return the original service registration, never <code>null</code>.
         */
        public ServiceRegistration<T> getRegistration() {
            return m_registration;
        }
    }

    /**
     * Provides a tenant-aware bundle event wrapper.
     */
    private class TenantAwareBundleEvent extends BundleEvent {

        private static final long serialVersionUID = 1L;

        public TenantAwareBundleEvent(BundleEvent event) {
            super(event.getType(), event.getBundle());
        }

        @Override
        public Bundle getBundle() {
            return getTenantAwareBundle(super.getBundle());
        }
    }

    /**
     * Provides a tenant-aware bundle listener.
     */
    private class TenantAwareBundleListener implements BundleListener {

        private final BundleListener m_listener;

        public TenantAwareBundleListener(BundleListener listener) {
            m_listener = listener;
        }

        @Override
        public void bundleChanged(BundleEvent event) {
            if (bundleVisibleForTenant(event.getBundle())) {
                m_listener.bundleChanged(new TenantAwareBundleEvent(event));
            }
        }
    }

    /**
     * Provides a tenant-aware framework listener.
     */
    private static class TenantAwareFrameworkListener implements FrameworkListener {

        private final FrameworkListener m_listener;

        public TenantAwareFrameworkListener(FrameworkListener listener) {
            m_listener = listener;
        }

        @Override
        public void frameworkEvent(FrameworkEvent event) {
            m_listener.frameworkEvent(new ScopedFrameworkEvent(event));
        }
    }

    private final ConcurrentMap<BundleListener, TenantAwareBundleListener> m_bundleListeners =
        new ConcurrentHashMap<BundleListener, TenantAwareBundleListener>();
    private final ConcurrentMap<FrameworkListener, TenantAwareFrameworkListener> m_frameworkListeners =
        new ConcurrentHashMap<FrameworkListener, TenantAwareFrameworkListener>();
    private final ConcurrentMap<ServiceRegistrationAdapter<?>, ServiceRegistration<?>> m_serviceRegistrations =
        new ConcurrentHashMap<ServiceRegistrationAdapter<?>, ServiceRegistration<?>>();

    private final BundleContext m_parentBundleContext;
    private final TenantAdapter m_tenantAdapter;
    private final String m_lookupFilter;
    private final String m_bundleScope;

    /**
     * Creates a new {@link TenantAwareBundleContext}.
     * 
     * @param bundleContext the original bundle context to wrap;
     * @param tenantAdapter the tenant adapter that create this bundle context
     * @param lookupFilter the (optional) service lookup filter to use for this context;
     * @param bundleScope
     */
    public TenantAwareBundleContext(BundleContext bundleContext, TenantAdapter tenantAdapter, String lookupFilter,
        String bundleScope) {

        if (bundleContext == null) {
            throw new IllegalArgumentException("BundleContext cannot be null!");
        }
        m_parentBundleContext = bundleContext;

        if (tenantAdapter == null) {
            throw new IllegalArgumentException("TenantAdapter cannot be null!");
        }
        m_tenantAdapter = tenantAdapter;

        if (lookupFilter != null) {
            m_lookupFilter = lookupFilter;
        }
        else {
            m_lookupFilter = "";
        }

        m_bundleScope = bundleScope;
    }

    @Override
    public void addBundleListener(BundleListener listener) {
        TenantAwareBundleListener scopedBundleListener = new TenantAwareBundleListener(listener);
        if (m_bundleListeners.putIfAbsent(listener, scopedBundleListener) == null) {
            m_parentBundleContext.addBundleListener(scopedBundleListener);
        }
    }

    @Override
    public void addFrameworkListener(FrameworkListener listener) {
        TenantAwareFrameworkListener scopedFrameworkListener = new TenantAwareFrameworkListener(listener);
        if (m_frameworkListeners.putIfAbsent(listener, scopedFrameworkListener) == null) {
            m_parentBundleContext.addFrameworkListener(scopedFrameworkListener);
        }
    }

    @Override
    public void addServiceListener(ServiceListener listener) {
        try {
            m_parentBundleContext.addServiceListener(listener, getScopedLookupFilter(null));
        }
        catch (InvalidSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addServiceListener(ServiceListener listener, String filter) throws InvalidSyntaxException {
        m_parentBundleContext.addServiceListener(listener, getScopedLookupFilter(filter));
    }

    @Override
    public Filter createFilter(String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.createFilter(filter);
    }

    @Override
    public ServiceReference<?>[] getAllServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.getAllServiceReferences(clazz, getScopedLookupFilter(filter));
    }

    @Override
    public Bundle getBundle() {
        return new TenantAwareBundle(this);
    }

    @Override
    public Bundle getBundle(long id) {
        Bundle bundle = m_parentBundleContext.getBundle(id);
        return getTenantAwareBundle(bundle);
    }

    @Override
    public Bundle getBundle(String location) {
        Bundle bundle = m_parentBundleContext.getBundle(location);
        return getTenantAwareBundle(bundle);
    }

    @Override
    public Bundle[] getBundles() {
        Bundle[] bundles = m_parentBundleContext.getBundles();
        if (bundles == null) {
            return new Bundle[0];
        }

        List<Bundle> bundleList = new ArrayList<Bundle>();
        for (int i = 0; i < bundles.length; i++) {
            if (bundleVisibleForTenant(bundles[i])) {
                bundleList.add(getTenantAwareBundle(bundles[i]));
            }
        }
        return bundleList.toArray(new Bundle[0]);
    }

    @Override
    public File getDataFile(String filename) {
        BundleDataStore dataStore = m_tenantAdapter.getBundleDataStore(m_parentBundleContext);
        if (dataStore == null) {
            return null;
        }
        File dataFile = dataStore.getRoot();
        if (dataFile == null) {
            return null;
        }
        if (!dataFile.exists()) {
            dataFile.mkdir();
        }
        if (filename == null || "".equals(filename)) {
            return dataFile;
        }
        return new File(dataFile, filename);
    }

    /**
     * Returns the value of the specified property. If the property is not found in the tenant properties
     * this call is delegated to the parent BundleContext
     */
    @Override
    public String getProperty(String key) {
        Object object = m_tenantAdapter.getTenant().getProperties().get(key);
        if (object != null) {
            return object.toString();
        }
        return m_parentBundleContext.getProperty(key);
    }

    @Override
    public <S> S getService(ServiceReference<S> reference) {
        return m_parentBundleContext.getService(reference);
    }

    @Override
    public ServiceReference<?> getServiceReference(String clazz) {
        try {
            ServiceReference<?>[] references =
                m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(null));
            if (references != null && references.length > 0) {
                Arrays.sort(references);
                return references[0];
            }
        }
        catch (InvalidSyntaxException e) {
            // Ignore...
        }
        return null;
    }

    @Override
    public <S> ServiceReference<S> getServiceReference(Class<S> clazz) {
        try {
            Collection<ServiceReference<S>> matches =
                m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(null));
            if (!matches.isEmpty()) {
                @SuppressWarnings("unchecked")
                ServiceReference<S>[] references = matches.toArray(new ServiceReference[matches.size()]);
                Arrays.sort(references);
                return references[0];
            }
        }
        catch (InvalidSyntaxException e) {
            // Ignore...
        }
        return null;
    }

    @Override
    public ServiceReference<?>[] getServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(filter));
    }

    @Override
    public <S> Collection<ServiceReference<S>> getServiceReferences(Class<S> clazz, String filter)
        throws InvalidSyntaxException {
        return m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(filter));
    }

    @Override
    public Bundle installBundle(String location) throws BundleException {
        try {
            return m_parentBundleContext.installBundle(location,
                new MultiTenantBundleInputStream((new URL(location)).openStream()));
        }
        catch (IOException e) {
            throw new BundleException(
                "Could not convert bundle location to an input stream, wrapping it failed.", e);
        }
    }

    @Override
    public Bundle installBundle(String location, InputStream input) throws BundleException {
        return m_parentBundleContext.installBundle(location, new MultiTenantBundleInputStream(input));
    }

    @Override
    public <S> ServiceRegistration<S> registerService(Class<S> clazz, S service, Dictionary<String, ?> properties) {
        Dictionary<String, ?> scopedProperties = getScopedProperties(properties, clazz.getName());
        ServiceRegistration<S> registration = m_parentBundleContext.registerService(clazz, service, scopedProperties);
        ServiceRegistrationAdapter<S> adapter = new ServiceRegistrationAdapter<S>(this, registration);
        m_serviceRegistrations.putIfAbsent(adapter, registration);
        return adapter;
    }

    @Override
    public ServiceRegistration<?> registerService(String clazz, Object service, Dictionary<String, ?> properties) {
        Dictionary<String, ?> scopedProperties = getScopedProperties(properties, clazz);
        ServiceRegistration<?> registration = m_parentBundleContext.registerService(clazz, service, scopedProperties);
        @SuppressWarnings({ "rawtypes", "unchecked" })
        ServiceRegistrationAdapter<?> adapter = new ServiceRegistrationAdapter(this, registration);
        m_serviceRegistrations.putIfAbsent(adapter, registration);
        return adapter;
    }

    @Override
    public ServiceRegistration<?> registerService(String[] clazzes, Object service, Dictionary<String, ?> properties) {
        Dictionary<String, ?> scopedProperties = getScopedProperties(properties, clazzes);
        ServiceRegistration<?> registration = m_parentBundleContext.registerService(clazzes, service, scopedProperties);
        @SuppressWarnings({ "rawtypes", "unchecked" })
        ServiceRegistrationAdapter<?> adapter = new ServiceRegistrationAdapter(this, registration);
        m_serviceRegistrations.putIfAbsent(adapter, registration);
        return adapter;
    }

    @Override
    public void removeBundleListener(BundleListener listener) {
        TenantAwareBundleListener scopedBundleListener = m_bundleListeners.remove(listener);
        if (scopedBundleListener != null) {
            m_parentBundleContext.removeBundleListener(scopedBundleListener);
        }
    }

    @Override
    public void removeFrameworkListener(FrameworkListener listener) {
        TenantAwareFrameworkListener scopedFrameworkListener = m_frameworkListeners.remove(listener);
        if (scopedFrameworkListener != null) {
            m_parentBundleContext.removeFrameworkListener(scopedFrameworkListener);
        }
    }

    @Override
    public void removeServiceListener(ServiceListener listener) {
        m_parentBundleContext.removeServiceListener(listener);
    }

    @Override
    public String toString() {
        return "TenantAwareBundleContext(" + m_tenantAdapter.getTenant().getPID() + ")";
    }

    @Override
    public boolean ungetService(ServiceReference<?> reference) {
        try {
            return m_parentBundleContext.ungetService(reference);
        }
        catch (Exception e) {
            // Ignore...
        }
        return false;
    }

    /**
     * Handle change in tenant properties.
     * 
     * In case the BundleScope is TENANT simulate BundleEvents for bundles that
     * 
     * <ul>
     * <li> Became visible to the tenant
     * <li> Are not visible to the tenant anymore
     * </ul>
     * 
     * @param oldTenantProperties tenant properties from before the tenant has changed
     * @param newTenantProperties tenant properties after the change
     */
    void tenantChanged(Tenant oldTenant, Tenant newTenant) {

        if (!MULTITENANT_BUNDLE_SCOPE_TENANT.equals(m_bundleScope)) {
            /* Not TENANT BundleScope no need to simulate bundle events */
            return;
        }

        List<Bundle> bundles = filterBundles(m_parentBundleContext.getBundles(), oldTenant);
        List<Bundle> newBundles = filterBundles(m_parentBundleContext.getBundles(), newTenant);

        List<Bundle> removeBundles = new ArrayList<Bundle>(bundles);
        removeBundles.removeAll(newBundles);

        List<Bundle> addBundles = new ArrayList<Bundle>(newBundles);
        addBundles.removeAll(bundles);

        for (Iterator<TenantAwareBundleListener> iterator = m_bundleListeners.values().iterator(); iterator.hasNext();) {
            TenantAwareBundleListener listener = iterator.next();

            for (Bundle remove : removeBundles) {
                /*
                 * Use the original BundleListener instead of the TenantAwareBundleListener as the bundle is already wrapped
                 * in a TenantAwareBundle to override the Bundle state
                 */
                listener.m_listener.bundleChanged(new BundleEvent(BundleEvent.UNINSTALLED, new TenantAwareBundle(
                    remove, Bundle.UNINSTALLED)));
            }

            for (Bundle add : addBundles) {
                // TODO: Send event based on the current state of the bundle here?
                listener.bundleChanged(new BundleEvent(BundleEvent.INSTALLED, add));
            }
        }
    }

    /**
     * Returns the <em>original</em> (i.e., the one that was used to create the bundle) bundle context.
     * <p>
     * USE WITH CARE, THIS BUNDLE CONTEXT GOES OUTSIDE THE MULTI-TENANCY CONCEPT!
     * </p>
     * 
     * @return the original bundle context, never <code>null</code>.
     */
    final BundleContext getParent() {
        return m_parentBundleContext;
    }

    /**
     * Returns a {@code TenantAwareBundle} for the specified {@code Bundle}.
     * 
     * @param bundle a regular bundle
     * @return the tenant aware bundle
     */
    private Bundle getTenantAwareBundle(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        BundleContext bundleContext = bundle.getBundleContext();
        if (bundleContext != null) {
            TenantAwareBundleContext scopedBundleContext = createScopedBundleContext(bundleContext);
            return scopedBundleContext.getBundle();
        }
        return new TenantAwareBundle(bundle);
    }

    /**
     * Adapts (or creates) the given dictionary in such way that it always contains the tenant.pid property.
     * 
     * @param properties the original dictionary, may be null, in which case a new dictionary will be created.
     * @return the scoped dictionary, never <code>null</code>.
     */
    private Dictionary<String, ?> getScopedProperties(ServiceRegistration<?> serviceRegistration,
        Dictionary<String, ?> properties) {

        Dictionary<String, Object> scopedProperties = new Hashtable<String, Object>();
        if (properties != null) {
            Enumeration<String> keys = properties.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                scopedProperties.put(key, properties.get(key));
            }
        }

        // Make sure the identifier of this tenant is registered with *all*
        // services the tenant-aware service registers itself...
        scopedProperties.put(PID_KEY, m_tenantAdapter.getTenant().getPID());
        return scopedProperties;
    }

    /**
     * Crafts a service-property {@link Dictionary} based on the given {@link Dictionary} and
     * correctly scoped regarding the visibility of the service (if necessary).
     * 
     * @param properties the original service properties;
     * @param serviceClasses the array with service classes the service will be registered under.
     */
    private Dictionary<String, ?> getScopedProperties(Dictionary<String, ?> properties,
        String... serviceClasses) {

        Dictionary<String, Object> scopedProperties = new Hashtable<String, Object>();
        if (properties != null) {
            Enumeration<String> keys = properties.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                scopedProperties.put(key, properties.get(key));
            }
        }

        // Make sure the identifier of this tenant is registered with *all*
        // services the tenant-aware service registers itself...
        scopedProperties.put(PID_KEY, m_tenantAdapter.getTenant().getPID());
        return scopedProperties;
    }

    /**
     * Unregisters the given service registration adapter.
     * 
     * @param serviceReg the service registration adapter to unregister, cannot be <code>null</code>.
     */
    private void unregisterService(ServiceRegistrationAdapter<?> serviceReg) {
        m_serviceRegistrations.remove(serviceReg);
        serviceReg.getRegistration().unregister();
    }

    /**
     * Creates a filter for service-lookups that uses the correct scoping.
     * 
     * @param filter the input filter to scope, can be <code>null</code> or empty.
     * @return a filter clause, never <code>null</code>.
     */
    private String getScopedLookupFilter(String filter) {
        if (filter == null || "".equals(filter)) {
            return m_lookupFilter;
        }
        return "(&" + filter + m_lookupFilter + ")";
    }

    /**
     * Returns the subset of the specified bundles visible to the specified tenant.
     * 
     * @param bundles Array of bundles to consider
     * @param tenant The tenant
     * @return A list of bundles visible to the tenant
     */
    private List<Bundle> filterBundles(Bundle[] bundles, Tenant tenant) {
        List<Bundle> filtered = new ArrayList<>();
        for (Bundle bundle : bundles) {
            if (bundleVisibleForTenant(bundle, tenant)) {
                filtered.add(bundle);
            }
        }
        return filtered;
    }

    /**
     * Creates a bundle context that is scoped regarding the visibility, data store and lookup.
     * 
     * @param bundleContext the bundle context to wrap, cannot be <code>null</code>.
     * @return a new {@link TenantAwareBundleContext} instance, never <code>null</code>.
     */
    private TenantAwareBundleContext createScopedBundleContext(BundleContext bundleContext) {
        // TODO: Should we get the BundleScope value from the adapter? To be able to do this we'll need to lookup the adapter for the target bundle first
        String bundleScope = (String) bundleContext.getBundle().getHeaders().get(MULTITENANT_BUNDLE_SCOPE_KEY);
        return new TenantAwareBundleContext(bundleContext, m_tenantAdapter, m_lookupFilter, bundleScope);
    }

    /**
     * Returns whether a bundle should be visible to the current tenant
     * 
     * @param bundle the bundle
     * @param tenant the tenant
     * @return <code>true</code> if the bundle bundle should be visible to the tenant
     *         <code>false</code> otherwise.
     */
    private boolean bundleVisibleForTenant(Bundle bundle) {
        return bundleVisibleForTenant(bundle, m_tenantAdapter.getTenant());
    }

    /**
     * Returns whether a bundle should be visible to a specific tenant
     * 
     * @param bundle the bundle
     * @param tenant the tenant
     * @return <code>true</code> if the bundle bundle should be visible to the tenant
     *         <code>false</code> otherwise.
     */
    private boolean bundleVisibleForTenant(Bundle bundle, Tenant tenant) {
        if (m_bundleScope.equals(MULTITENANT_BUNDLE_SCOPE_FRAMEWORK)) {
            return true;
        }
        else if (m_bundleScope.equals(MULTITENANT_BUNDLE_SCOPE_TENANT)) {
            if (!isMultiTenantAwareBundle(bundle)) {
                return false;
            }

            try {
                Filter bindingFilter = getBindingFilter(bundle);
                return bindingFilter.matches(tenant.getProperties());
            }
            catch (InvalidSyntaxException e) {
                // Ignore...
            }
            return true;
        }
        else {
            throw new IllegalArgumentException("Unsupported bundle scope value " + m_bundleScope);
        }
    }

}
