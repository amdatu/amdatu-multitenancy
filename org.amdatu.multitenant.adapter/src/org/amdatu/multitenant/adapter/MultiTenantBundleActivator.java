/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import static org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_KEY;
import static org.amdatu.multitenant.adapter.Utils.getBindingFilter;
import static org.amdatu.multitenant.adapter.Utils.isMultiTenantAwareBundle;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.service.log.LogService;

/**
 * Generic bundle activator that can be used to convert a normal bundle into a multi tenant one.
 * The process of converting a normal bundle starts with replacing the existing bundle activator
 * with this one. Via a set of extra manifest entries the normal bundle activator can be specified.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @see Constants
 */
public class MultiTenantBundleActivator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        // Only in case the correct marker bundle headers are found, we process this bundle further.
        // If those markers are not found, we don't do anything, assuming that somebody else might
        // be able to pick it up...
        if (isMultiTenantAwareBundle(context.getBundle())) {

            Filter bindingFilter = getBindingFilter(context.getBundle());

            Dictionary<String, Object> props = new Hashtable<String, Object>();
            props.put(LIFECYCLELISTENER_SCOPE_KEY, bindingFilter);

            manager.add(createComponent()
                .setInterface(TenantLifeCycleListener.class.getName(), props)
                .setImplementation(MultiTenantBundleDataStore.class)
                .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false)
                )
                );

            // For the individual tenants, we create an adapter that is called for each individually registered tenant.
            manager.add(createAdapterService(Tenant.class, bindingFilter.toString(), "added", "changed", "removed")
                .setImplementation(TenantAdapter.class)
                .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false)
                )
                );
        }
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

}
