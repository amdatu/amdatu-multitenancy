/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.Version;

/**
 * Wrapper class for the bundle interface to make it tenant aware.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantAwareBundle implements Bundle {

    private final TenantAwareBundleContext m_scopedBundleContext;
    private final Bundle m_parentBundle;
    private int m_stateOverride = -1;

    public TenantAwareBundle(TenantAwareBundleContext context, Bundle parentBundle) {
        m_scopedBundleContext = context;
        m_parentBundle = parentBundle;
    }

    public TenantAwareBundle(TenantAwareBundleContext context) {
        this(context, context.getParent().getBundle());
    }

    public TenantAwareBundle(Bundle bundle) {
        this(null, bundle);
    }

    public TenantAwareBundle(Bundle bundle, int state) {
        this(null, bundle);
        m_stateOverride = state;
    }

    /*
     * Public access
     */

    public Bundle getParent() {
        return m_parentBundle;
    }

    /*
     * Bundle interface
     */

    @Override
    public <A extends Object> A adapt(java.lang.Class<A> type) {
        return getParent().adapt(type);
    }

    @Override
    public int getState() {
        if (m_stateOverride != -1) {
            return m_stateOverride;
        }

        return getParent().getState();
    }

    @Override
    public void start(int options) throws BundleException {
        getParent().start(options);
    }

    @Override
    public void start() throws BundleException {
        getParent().start();
    }

    @Override
    public void stop(int options) throws BundleException {
        getParent().stop(options);
    }

    @Override
    public void stop() throws BundleException {
        getParent().stop();
    }

    @Override
    public void update(InputStream input) throws BundleException {
        getParent().update(input);
    }

    @Override
    public void update() throws BundleException {
        getParent().update();
    }

    @Override
    public void uninstall() throws BundleException {
        getParent().uninstall();
    }

    @Override
    public Dictionary<String, String> getHeaders() {
        return getParent().getHeaders();
    }

    @Override
    public long getBundleId() {
        return getParent().getBundleId();
    }

    @Override
    public String getLocation() {
        return getParent().getLocation();
    }

    @Override
    public ServiceReference<?>[] getRegisteredServices() {
        return getParent().getRegisteredServices();
    }

    @Override
    public ServiceReference<?>[] getServicesInUse() {
        return getParent().getServicesInUse();
    }

    @Override
    public boolean hasPermission(Object permission) {
        return getParent().hasPermission(permission);
    }

    @Override
    public URL getResource(String name) {
        return getParent().getResource(name);
    }

    @Override
    public Dictionary<String, String> getHeaders(String locale) {
        return getParent().getHeaders(locale);
    }

    @Override
    public String getSymbolicName() {
        return getParent().getSymbolicName();
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return getParent().loadClass(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        return getParent().getResources(name);
    }

    @Override
    public Enumeration<String> getEntryPaths(String path) {
        return getParent().getEntryPaths(path);
    }

    @Override
    public URL getEntry(String path) {
        return getParent().getEntry(path);
    }

    @Override
    public long getLastModified() {
        return getParent().getLastModified();
    }

    @Override
    public Enumeration<URL> findEntries(String path, String filePattern, boolean recurse) {
        return getParent().findEntries(path, filePattern, recurse);
    }

    @Override
    public BundleContext getBundleContext() {
        return m_scopedBundleContext;
    }

    @Override
    public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(int signersType) {
        return getParent().getSignerCertificates(signersType);
    }

    @Override
    public Version getVersion() {
        return getParent().getVersion();
    }

    @Override
    public File getDataFile(String filename) {
        if (m_scopedBundleContext == null) {
            throw new IllegalStateException("Scoped bundle context should not be null");
        }
        return m_scopedBundleContext.getDataFile(filename);
    }

    @Override
    public int compareTo(Bundle other) {
        if (other instanceof TenantAwareBundle) {
            return getParent().compareTo(((TenantAwareBundle) other).getParent());
        }
        return getParent().compareTo(other);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Bundle)) {
            return false;
        }

        if (obj instanceof TenantAwareBundle) {
            TenantAwareBundle other = (TenantAwareBundle) obj;
            return m_parentBundle.equals(other.m_parentBundle);
        }

        return m_parentBundle.equals(obj);
    }

    @Override
    public int hashCode() {
        return Long.valueOf(m_parentBundle.getBundleId()).hashCode();
    }
}
