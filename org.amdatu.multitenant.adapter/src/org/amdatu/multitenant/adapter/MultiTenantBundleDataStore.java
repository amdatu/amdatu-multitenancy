/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * Implements a tenants specific data store for bundles, using the life cycle listener.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MultiTenantBundleDataStore implements TenantLifeCycleListener {

    private final Map<String, Component> m_services = new HashMap<String, Component>();
    private final Set<String> m_tenants = new HashSet<String>();

    private volatile BundleContext m_context;
    private volatile DependencyManager m_manager;
    private volatile LogService m_log;

    private File m_root;

    public void start() {
        // list all existing files, assuming they're all existing tenants
        m_root = m_context.getDataFile("");
        File[] files = m_root.listFiles();
        for (File file : files) {
            m_tenants.add(file.getName());
        }
    }

    public void stop() {
        for (Entry<String, Component> entry : m_services.entrySet()) {
            m_manager.remove(entry.getValue());
        }
        m_services.clear();
        m_tenants.clear();
    }

    @Override
    public void initial(Tenant[] tenants) {
        Set<String> pids = new HashSet<String>();
        for (Tenant tenant : tenants) {
            pids.add(tenant.getPID());
            create(tenant);
        }
        for (String tenantPid : m_tenants) {
            if (!pids.contains(tenantPid)) {
                deleteStore(tenantPid);
            }
        }
    }

    @Override
    public void create(Tenant tenant) {
        File tenantRoot = new File(m_root, tenant.getPID());
        if (!tenantRoot.isDirectory() && !tenantRoot.mkdir()) {
            m_log.log(LogService.LOG_WARNING,
                "Failed to create tenant data storage area: " + tenantRoot.getAbsolutePath());
            return;
        }

        Properties props = new Properties();
        props.put(Constants.PID_KEY, tenant.getPID());
        props.put(org.amdatu.multitenant.adapter.Constants.BUNDLE_ID, m_context.getBundle().getBundleId());
        Component component = m_manager.createComponent()
            .setInterface(BundleDataStore.class.getName(), props)
            .setImplementation(new BundleDataStoreImpl(tenantRoot));
        boolean added = false;
        synchronized (m_services) {
            if (!m_services.containsKey(tenant.getPID())) {
                m_services.put(tenant.getPID(), component);
                added = true;
            }
        }
        if (added) {
            m_manager.add(component);
        }
    }

    @Override
    public void update(Tenant tenant) {
        // PID does not change
    }

    @Override
    public void delete(Tenant tenant) {
        Component component;
        synchronized (m_services) {
            component = m_services.remove(tenant.getPID());
        }
        if (component != null) {
            m_manager.remove(component);
        }
        deleteStore(tenant.getPID());
    }

    private void deleteStore(String tenantPID) {
        File tenantRoot = new File(m_root, tenantPID);
        if (tenantRoot.isDirectory()) {
            try {
                deleteDir(tenantRoot);
            }
            catch (IOException e) {
                m_log.log(LogService.LOG_WARNING,
                    "Failed to delete tenant data storage area: " + tenantRoot.getAbsolutePath(), e);
            }
        }
    }

    private void deleteDir(File dir) throws IOException {
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                deleteDir(file);
            }
            else {
                if (!file.delete()) {
                    throw new IOException("Could not delete file " + file.getAbsolutePath());
                }
            }
        }
        dir.delete();
    }
}
