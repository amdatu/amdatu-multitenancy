/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant;

/**
 * Compile time constants for multi tenancy.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Constants {

    /**
     * The service-property used to distinguish different {@link Tenant}s.
     */
    String PID_KEY = "org.amdatu.tenant.pid";

    /**
     * An optional service-property used to give a logical name to a {@link Tenant}.
     */
    String NAME_KEY = "org.amdatu.tenant.name";

    /**
     * The PID of the platform tenant.
     */
    String PID_VALUE_PLATFORM = "org.amdatu.tenant.PLATFORM";

    /**
     * The (human readable) name of the platform tenant.
     */
    String NAME_VALUE_PLATFORM = "Platform Tenant";

    /**
     * Optional scope filter property for the life cycle listener service properties. The
     * property value must be of type String or Filter and represent a valid filter.
     */
    String LIFECYCLELISTENER_SCOPE_KEY = "org.amdatu.tenant.listenerscope";

    /**
     * Scope filter value for listeners that are only interested in platform tenant events.
     */
    String LIFECYCLELISTENER_SCOPE_VALUE_PLATFORM = String.format("(%s=%s)", org.amdatu.multitenant.Constants.PID_KEY,
        org.amdatu.multitenant.Constants.PID_VALUE_PLATFORM);

    /**
     * Scope filter value for listeners that are interested in all regular tenant events.
     */
    String LIFECYCLELISTENER_SCOPE_VALUE_TENANTS = String.format("(!%s)", LIFECYCLELISTENER_SCOPE_VALUE_PLATFORM);
}
