/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant;

/**
 * Tenant life cycle listener. When a tenant is created or deleted, the life cycle
 * listeners are invoked. A component that is interested in listening to the creation
 * and deletion life cycle of a tenant can register whiteboard style with this interface.
 * Optionally, it can register with a scope filter that allows it to specify if it only
 * wants to hear about the platform, the other tenants or both.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TenantLifeCycleListener {

    /**
     * Initial notification of the already created tenants at the moment this listener
     * is registered.
     * 
     * @param tenants list of tenants
     */
    void initial(Tenant[] tenants);

    /**
     * Notification that a new tenant is created.
     * 
     * @param tenant the tenant
     */
    void create(Tenant tenant);

    /**
     * Notification that an existing tenant tenant is updated.
     * 
     * @param tenant the tenant
     */
    void update(Tenant tenant);

    /**
     * Notification that an existing tenant is deleted.
     * 
     * @param tenant the tenant
     */
    void delete(Tenant tenant);
}
