/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.fail;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

public class TenantConfigurator {

	private volatile TenantFactoryConfiguration m_tenantFactoryConfiguration;
	private volatile BundleContext m_bundleContext;

	public void configureTenants(String... pids) {
		Map<String, Map<String, Object>> tenants = new HashMap<String, Map<String, Object>>();
		for (String pid : pids) {
			String name = "Tentant " + pid;
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(org.amdatu.multitenant.Constants.PID_KEY, pid);
			props.put(org.amdatu.multitenant.Constants.NAME_KEY, name);
			tenants.put(pid, props);
		}
		TenantFactoryConfiguration tfc = m_tenantFactoryConfiguration;
		if (tfc != null) {
			tfc.update(tenants);
		} else {
			Assert.fail("No tenant factory configuration service found.");
		}
	}

	public void configureTenants(TenantWithProperties... tenants) {
		Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
		for (TenantWithProperties tenant : tenants) {
			result.put(tenant.getPid(), tenant.getProperties());
		}
		TenantFactoryConfiguration tfc = m_tenantFactoryConfiguration;
		if (tfc != null) {
			tfc.update(result);
		} else {
			Assert.fail("No tenant factory configuration service found.");
		}
	}
	/**
	 * Updates the configuration for the {@link ManagedService} identified by
	 * the given PID to the given properties.
	 * 
	 * @param pid
	 *            the PID of the {@link ManagedService} to update, cannot be
	 *            <code>null</code>;
	 * @param properties
	 *            the new service properties, can be <code>null</code> to delete
	 *            the existing service properties (if any).
	 * @return the updated configuration information, never <code>null</code>.
	 * @throws IOException
	 *             if the configuration was rejected or otherwise caused an
	 *             exception.
	 */
	public Configuration updateConfig(String pid, Properties properties)
			throws IOException {
		return updateConfig(pid, properties, false /* factory */);
	}

	/**
	 * Updates the configuration for the {@link ManagedServiceFactory}
	 * identified by the given PID to the given properties.
	 * 
	 * @param pid
	 *            the PID of the {@link ManagedServiceFactory} to update, cannot
	 *            be <code>null</code>;
	 * @param properties
	 *            the new service properties, can be <code>null</code> to delete
	 *            the existing service properties (if any).
	 * @return the updated configuration information, never <code>null</code>.
	 * @throws IOException
	 *             if the configuration was rejected or otherwise caused an
	 *             exception.
	 */
	public Configuration updateFactoryConfig(String pid, Properties properties)
			throws IOException {
		return updateConfig(pid, properties, true /* factory */);
	}

	private Configuration updateConfig(String pid, Properties properties,
			boolean factory) throws IOException {
		ConfigurationAdmin configurationAdmin = getConfigAdminService(getConfigAdminServiceRef());

		Configuration configuration;
		if (!factory) {
			configuration = configurationAdmin
					.getConfiguration(pid, null /* location */);
		} else {
			configuration = configurationAdmin.createFactoryConfiguration(pid,
					null /* location */);
		}
		Dictionary d = properties;
		configuration.update(d);

		return configuration;
	}
	
	 /**
     * Obtains the {@link ConfigurationAdmin} service through the given service reference.
     * 
     * @param sr the service reference of the {@link ConfigurationAdmin} service, cannot be <code>null</code>.
     * @return the {@link ConfigurationAdmin} service, never <code>null</code>.
     * @throws AssertionFailedError in case the requested service was not available.
     */
    private ConfigurationAdmin getConfigAdminService(ServiceReference sr) {
        ConfigurationAdmin configurationAdmin = (ConfigurationAdmin) m_bundleContext.getService(sr);
        assertNotNull(configurationAdmin);
        return configurationAdmin;
    }

    /**
     * Obtains the {@link ConfigurationAdmin} service reference.
     * 
     * @return the {@link ConfigurationAdmin} service reference, never <code>null</code>.
     * @throws AssertionFailedError in case the requested service reference was not available.
     */
    private ServiceReference getConfigAdminServiceRef() {
        ServiceReference[] refs = null;
        try {
            refs = m_bundleContext.getServiceReferences(ConfigurationAdmin.class.getName(), "(" + org.amdatu.multitenant.Constants.PID_KEY + "=" + org.amdatu.multitenant.Constants.PID_VALUE_PLATFORM + ")");
        }
        catch (InvalidSyntaxException e) {
            throw new RuntimeException(e);
        }
        assertNotNull(refs);
        return refs[0];
    }
	 public void waitForSystemToSettle() {
	        try {
	            Thread.sleep(250);
	        }
	        catch (InterruptedException exception) {
	            fail("Interrupted while waiting for configuration!");
	        }
	    }
	
	
}
