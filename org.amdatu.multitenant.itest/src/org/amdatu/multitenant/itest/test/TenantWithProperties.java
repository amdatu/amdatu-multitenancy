/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.test;

import java.util.HashMap;
import java.util.Map;

public class TenantWithProperties {
	private final String m_pid;
	private final Map<String, Object> m_properties;

	public TenantWithProperties(String pid, Object... properties) {
		m_pid = pid;
		m_properties = new HashMap<String, Object>();
		for (int i = 0; i < properties.length; i += 2) {
			m_properties.put((String) properties[i], properties[i + 1]);
		}
	}

	public String getPid() {
		return m_pid;
	}

	public Map<String, Object> getProperties() {
		return m_properties;
	}
}