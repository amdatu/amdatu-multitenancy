/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.test;

import static org.amdatu.multitenant.Constants.NAME_KEY;
import static org.amdatu.multitenant.Constants.PID_KEY;
import static org.amdatu.multitenant.Constants.PID_VALUE_PLATFORM;
import static org.amdatu.multitenant.adapter.Constants.BUNDLE_ID;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.amdatu.multitenant.adapter.BundleDataStore;
import org.amdatu.multitenant.adapter.TenantAwareBundleContext;
import org.amdatu.multitenant.itest.MyDependencyService;
import org.amdatu.multitenant.itest.MyDependentService;
import org.amdatu.multitenant.itest.MyGlobalService;
import org.amdatu.multitenant.itest.TenantAwareBundleContextService;
import org.amdatu.testing.configurator.TestConfigurator;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.util.tracker.BundleTracker;
import org.osgi.util.tracker.BundleTrackerCustomizer;

/**
 * Integration test cases for multi-tenancy.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */

public class MultiTenantTest extends TestCase {
    private static final String SERVICE_NAME = MyDependentService.class.getName();

    private volatile DependencyManager m_manager;
    private final List<Configuration> m_configurations = new ArrayList<Configuration>();
    private TenantConfigurator m_tenantConfigurator;
    private final BundleContext m_bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

    /**
     * Helper method to count the number of service registrations for a given service-class.
     *
     * @param context the bundle context to use;
     * @param service the service to count;
     * @param filter the optional filter to search for, can be <code>null</code>.
     * @return a service registration count, >= 0.
     */
    static int countServices(BundleContext context, String serviceName, String filter) {
        ServiceReference[] serviceReferences = null;
        try {
            serviceReferences = context.getServiceReferences(serviceName, filter);
        }
        catch (InvalidSyntaxException exception) {
            throw new RuntimeException("null-filter is incorrect?!");
        }
        return serviceReferences == null ? 0 : serviceReferences.length;
    }

    /**
     * Helper method to count the number of service registrations for a given service-class.
     *
     * @param context the bundle context to use;
     * @param service the service to count.
     * @return a service registration count, >= 0.
     */
    static int countServices(BundleContext context, String serviceName) {
        return countServices(context, serviceName, null /* filter */);
    }

    public void setUp() throws Exception {
        TestConfigurator.configure(this)
            .add(createServiceDependency().setService(TenantFactoryConfiguration.class).setRequired(true)).apply();
        
        m_tenantConfigurator = new TenantConfigurator();
        m_manager.add(m_manager.createComponent().setImplementation(m_tenantConfigurator)
            .add(m_manager.createServiceDependency().setService(TenantFactoryConfiguration.class).setRequired(true)));
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
    }

    /**
     * Tears down an individual test case.
     */
    public void tearDown() {
        for (Configuration config : m_configurations) {
            try {
                config.delete();
            }
            catch (Exception exception) {
                // Ignore...
            }
        }
        m_configurations.clear();
        TestConfigurator.cleanUp(this);
    }

    /**
     * Used to monitor the life cycle of tenants.
     */
    public static class Listener implements TenantLifeCycleListener {
        private final Set<Tenant> m_tenants = new HashSet<Tenant>();

        public Listener() {
        }

        public void initial(Tenant[] tenants) {
            synchronized (m_tenants) {
                m_tenants.clear();
                for (Tenant tenant : tenants) {
                    m_tenants.add(tenant);
                }
            }
        }

        public void create(Tenant tenant) {
            synchronized (m_tenants) {
                m_tenants.add(tenant);
            }
        }

        public void update(Tenant tenant) {
            synchronized (m_tenants) {
                m_tenants.remove(tenant);
                m_tenants.add(tenant);
            }
        }

        public void delete(Tenant tenant) {
            synchronized (m_tenants) {
                Tenant delete = null;
                for (Tenant t : m_tenants) {
                    if (t.getPID().equals(tenant.getPID())) {
                        delete = t;
                        break;
                    }
                }
                if (delete != null) {
                    m_tenants.remove(delete);
                }
            }
        }

        public String[] getTenants() {
            synchronized (m_tenants) {
                String[] result = new String[m_tenants.size()];
                int i = 0;
                for (Tenant t : m_tenants) {
                    result[i++] = t.getPID();
                }
                return result;
            }
        }
    }

    /**
     * Tests that the tenant life cycle matches the number of configurations that are added and removed.
     */
    public void testTenantLifeCycle() throws Exception {

        Listener initialListener = new Listener();
        Listener secondListener = new Listener();

        m_manager.add(m_manager.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), null)
            .setImplementation(initialListener));

        String pid1 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1);
        assertEquals(3, initialListener.getTenants().length);
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid1));

        String pid2 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1, pid2);
        assertEquals(4, initialListener.getTenants().length);
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid2));

        m_manager.add(m_manager.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), null)
            .setImplementation(secondListener));

        assertEquals(4, secondListener.getTenants().length);
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid2));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid2);
        assertEquals(3, initialListener.getTenants().length);
        assertFalse(Arrays.asList(initialListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid2));

        assertEquals(3, secondListener.getTenants().length);
        assertFalse(Arrays.asList(secondListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid2));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(2, initialListener.getTenants().length);
        assertFalse(Arrays.asList(initialListener.getTenants()).contains(pid2));
        assertEquals(2, secondListener.getTenants().length);
        assertFalse(Arrays.asList(secondListener.getTenants()).contains(pid2));
    }

    /**
     * Tests the persistency of a tenant lifecycle.
     *
     * @throws Exception not part of this test case.
     */
    public void testTenantLifeCyclePersistent() throws Exception {
        Listener listener = new Listener();

        DependencyManager dm = m_manager;
        Component component =
            dm.createComponent()
                .setInterface(TenantLifeCycleListener.class.getName(), null)
                .setImplementation(listener);
        dm.add(component);

        String pid1 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1);
        assertEquals(3, listener.getTenants().length);
        assertTrue(Arrays.asList(listener.getTenants()).contains(pid1));

        Bundle tenantFactory = null;
        for (Bundle bundle : m_bundleContext.getBundles()) {
            if ("org.amdatu.multitenant.factory".equals(bundle.getSymbolicName())) {
                tenantFactory = bundle;
            }
        }
        assertNotNull(tenantFactory);

        tenantFactory.stop();
        tenantFactory.start();

        //
        assertEquals(3, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        m_tenantConfigurator.waitForSystemToSettle();
        // but now we should
        assertEquals(2, listener.getTenants().length);
        Arrays.asList(listener.getTenants()).contains(pid1);
    }

    /** Tests a listener that has scope platform */
    public void testTenantLifeCycleListenerScopePlatform() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_KEY,
            org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_VALUE_PLATFORM);
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), properties)
            .setImplementation(listener);
        dm.add(component);

        assertEquals(1, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, generateTenantPID());
        assertEquals(1, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(generateTenantPID());
        assertEquals(0, listener.getTenants().length);
    }

    /** Tests a listener that has scope tenants */
    public void testTenantLifeCycleListenerScopeTenants() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_KEY,
            org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_VALUE_TENANTS);
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), properties)
            .setImplementation(listener);
        dm.add(component);

        assertEquals(1, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", generateTenantPID());
        assertEquals(2, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(1, listener.getTenants().length);
    }

    /** Tests a listener that has no scope filter */
    public void testTenantLifeCycleListenerScopeBoth() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), properties)
            .setImplementation(listener);
        dm.add(component);

        assertEquals(2, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", generateTenantPID());
        assertEquals(3, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(2, listener.getTenants().length);
    }

    /** Tests a listener that has scope PID=Q* */
    public void testTenantLifeCycleScopeCustom() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;

        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(org.amdatu.multitenant.Constants.LIFECYCLELISTENER_SCOPE_KEY,
            String.format("(%1s=%2s)", Constants.PID_KEY, "Q*"));
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), properties)
            .setImplementation(listener);
        dm.add(component);

        assertEquals(0, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", generateTenantPID());
        assertEquals(0, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", "Q123");
        assertEquals(1, listener.getTenants().length);
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testAddingMultiTenantAwareServiceCausesNewServiceRegistrations() throws Exception {
        String tenantPID = generateTenantPID();

        Properties properties = createTenantConfiguration(tenantPID);
        addTenantConfig(properties);

        // We've got a number of explicit tenants + one framework (= default) tenant
        int expectedServiceCount = getConfigurationCount();

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(expectedServiceCount, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testMultiTenantAwareServiceCausesMultipleServiceRegistrations() throws Exception {
        // We've got a number of explicit tenants + one framework (= default) tenant
        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(2, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that when a multi-tenant aware service is registered, each tenant only sees its own services, not the services of other tenants.
     *
     * @throws Exception not part of this test case.
     */
    public void testMultiTenantAwareServiceOnlySeesItsOwnServices() throws Exception {
        int newCount;

        String tenantPID = generateTenantPID();

        assertEquals(0, countServices(m_bundleContext, MyDependentService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID)));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        newCount = countServices(m_bundleContext, MyDependentService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID));
        assertEquals(1, newCount);

        newCount = countServices(m_bundleContext, MyDependencyService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID));
        assertEquals(1, newCount);
    }

    /**
     * Tests that you can retrieve the multi-tenant-aware <em>tenant</em> service if you explicitly ask for it.
     *
     * @throws Exception not part of this test case.
     */
    public void testObtainTenantSpecificServiceInstanceSucceedsForNonMultiTenantService() throws Exception {
        String tenantPID = generateTenantPID();

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(SERVICE_NAME,
            String.format("(%1$s=%2$s)", PID_KEY, tenantPID));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        MyDependentService service = (MyDependentService) m_bundleContext.getService(serviceRefs[0]);
        assertEquals(String.format("[%1$s] [%1$s]", tenantPID), service.sayIt());
    }

    /**
     * Tests that a global visible service is marked as such in its service properties.
     *
     * @throws Exception not part of this test case.
     */
    public void testGlobalVisibleServiceOk() throws Exception {
        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(),
            String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));

        serviceRefs = m_bundleContext.getServiceReferences(MyDependencyService.class.getName(),
            String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));
    }

    /**
     * Tests that updating the service properties of a globally visible service lets this service remain globally
     * visible (i.e., the global visibility flag is persistent).
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdateServicePropertiesOfGlobalVisibleServiceRemainsGloballyVisibleOk() throws Exception {
        String filter = String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM);

        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(), filter);
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        ServiceReference serviceReference = serviceRefs[0];
        assertEquals(null, serviceReference.getProperty("org.amdatu.tenant.global"));

        MyGlobalService service = (MyGlobalService) m_bundleContext.getService(serviceReference);

        ServiceRegistration serviceReg = service.getServiceRegistration();
        assertNotNull(serviceReg);

        Properties dict = new Properties();
        for (String key : serviceReference.getPropertyKeys()) {
            Object value = serviceReference.getProperty(key);
            // Copy all flags of the original service registration but not the global visibility flag...
            if (!"org.amdatu.tenant.global".equals(key)) {
                dict.put(key, value);
            }
        }

        // Update the service registration...
        serviceReg.setProperties(dict);

        // Wait a little while until the service is updated...
        TimeUnit.MILLISECONDS.sleep(250);

        serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(), filter);
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));
    }

    /**
     * Tests that you can retrieve the multi-tenant-aware <em>platform</em> service if you explicitly ask for it.
     *
     * @throws Exception not part of this test case.
     */
    public void testObtainPlatformSpecificServiceInstanceSucceedsForNonMultiTenantService() throws Exception {
        String tenantPID = generateTenantPID();

        Properties properties = createTenantConfiguration(tenantPID);
        addTenantConfig(properties);

        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(SERVICE_NAME,
            String.format("(%1$s=%2$s)", PID_KEY, Constants.PID_VALUE_PLATFORM));
        assertNotNull("Failed to obtain the MT-platform service?!", serviceRefs);
        assertEquals(1, serviceRefs.length);

        MyDependentService service = (MyDependentService) m_bundleContext.getService(serviceRefs[0]);
        assertEquals(String.format("[%1$s] [%1$s]", Constants.PID_VALUE_PLATFORM), service.sayIt());
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testRemovingMultiTenantAwareServiceCausesServiceDeregistrations() throws Exception {
        String tenantPID = generateTenantPID();

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));

        // Remove configuration; should cause service de-registration for tenant...
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(2, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that it is impossible to update the configuration for the PLATFORM-tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdatePlatformTenantImpossible() throws Exception {
        // We've got a number of explicit tenants + one framework (= default) tenant
        Properties properties = createTenantConfiguration(PID_VALUE_PLATFORM);
        Configuration config = addTenantConfig(properties);
        int expectedServiceCount = getConfigurationCount();

        // ??? configuration should actually be null?!
        assertNotNull(config);

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(expectedServiceCount, countServices(m_bundleContext, SERVICE_NAME));

        // Check that we can still find back our existing services...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + PID_KEY
            + "=" + Constants.PID_VALUE_PLATFORM + ")"));
    }

    /**
     * Tests that when a multi-tenant aware service is updated only its properties are updated.
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdateMultiTenantAwareServiceCausesServiceUpdates() throws Exception {
        String tenantPID = generateTenantPID();

        assertEquals(
            0,
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID)));

        String oldTenantName = "The old name";
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM),
            new TenantWithProperties("Default"),
            new TenantWithProperties(tenantPID, NAME_KEY, oldTenantName));

        // We've got a number of explicit tenants + one framework (= default) tenant
        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));

        // Verify the tenant can be found by its current name...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + PID_KEY
            + "=" + tenantPID + ")"));

        int countBefore =
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID));

        // Update the configuration...
        String newTenantName = "My cool new name";
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM),
            new TenantWithProperties("Default"),
            new TenantWithProperties(tenantPID, PID_KEY, "This-value-should-be-ignored", NAME_KEY, newTenantName));

        int countAfter =
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID));

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));
        assertEquals(countBefore, countAfter);

        // Check that we can find back our newly named tenant...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + NAME_KEY
            + "=" + newTenantName + ")"));
        // Check that the "old" tenant is really gone...
        assertEquals(0, countServices(m_bundleContext, Tenant.class.getName(), "(" + NAME_KEY
            + "=" + oldTenantName + ")"));
    }

    /**
     * Tests that tenant specific components can access the data storage in another bundle if (and only
     * if!) that bundle has a (scoped) bundle data store for the same tenant.
     * <br/><br/>
     * This test relies on 3 multi-tenant bundles that publish the TenantAwareBundleContextService
     * service with different scopes.
     * <table>
     * <thead>
     * <tr>
     * <td>BSN</td>
     * <td>Binding</td>
     * </tr>
     * </thead>
     * <tbody>
     * <tr>
     * <td>org.amdatu.multitenant.itest.datastoretest1</td>
     * <td>PLATFORM</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.datastoretest2</td>
     * <td>TENANT</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.datastoretest3</td>
     * <td>BOTH</td>
     * </tr>
     * </tbody>
     * </table>
     * 
     * @throws Exception
     * @see AMDATUMT-7
     */
    public void testTenantAwareBundleDataStoreAccess() throws Exception {

        String tenantPID = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        String someBundle = m_bundleContext.getBundle(1).getSymbolicName();

        // Multi-tenant context can access its own storage
        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest1",
            "org.amdatu.multitenant.itest.datastoretest1", Constants.PID_VALUE_PLATFORM);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest2",
            "org.amdatu.multitenant.itest.datastoretest2", tenantPID);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3",
            "org.amdatu.multitenant.itest.datastoretest3", Constants.PID_VALUE_PLATFORM);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3",
            "org.amdatu.multitenant.itest.datastoretest3", tenantPID);

        // Multi-tenant context can not access non multi-tenant storage under any circumstance

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest1", someBundle,
            Constants.PID_VALUE_PLATFORM);

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest2", someBundle,
            tenantPID);

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3", someBundle,
            Constants.PID_VALUE_PLATFORM);

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3", someBundle,
            tenantPID);

        // Multi-tenant context can not access multi-tenant storage without binding for the same tenant.

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest1",
            "org.amdatu.multitenant.itest.datastoretest2", Constants.PID_VALUE_PLATFORM);

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest2",
            "org.amdatu.multitenant.itest.datastoretest1", tenantPID);

        assertNoTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3",
            "org.amdatu.multitenant.itest.datastoretest1", tenantPID);

        // Multi-tenant context can access multi-tenant storage with binding for the same tenant.
        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest1",
            "org.amdatu.multitenant.itest.datastoretest3", Constants.PID_VALUE_PLATFORM);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3",
            "org.amdatu.multitenant.itest.datastoretest1", Constants.PID_VALUE_PLATFORM);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest2",
            "org.amdatu.multitenant.itest.datastoretest3", tenantPID);

        assertTenantAwareDataStoreAccess("org.amdatu.multitenant.itest.datastoretest3",
            "org.amdatu.multitenant.itest.datastoretest2", tenantPID);
    }

    private void assertTenantAwareDataStoreAccess(String fromBSN, String toBSN, String tenantPID) throws Exception {

        Bundle fromBundle = getBundleBySymbolicName(m_bundleContext, fromBSN);
        assertNotNull(fromBundle);

        BundleContext fromContext = getTenantAwareBundleContextFromBundle(fromBundle, tenantPID);
        assertNotNull(fromContext);

        Bundle toBundle = getBundleBySymbolicName(fromContext, toBSN);
        assertNotNull(toBundle);

        File toRoot = toBundle.getBundleContext().getDataFile("");
        assertNotNull(toRoot);

        String expectedPath = "bundle" + toBundle.getBundleId() + File.separator + "data" + File.separator + tenantPID;
        assertTrue("Expected data store path to end with: [" + expectedPath + "] but was ["
            + toRoot.getAbsolutePath() + "]", toRoot.getAbsolutePath().endsWith(expectedPath));

        File test = File.createTempFile("assertTenantAwareDataStoreAccess", ".tmp", toRoot);
        assertTrue(test.exists());
    }

    private void assertNoTenantAwareDataStoreAccess(String fromBSN, String toBSN, String tenantPID) throws Exception {

        Bundle fromBundle = getBundleBySymbolicName(m_bundleContext, fromBSN);
        assertNotNull(fromBundle);

        BundleContext fromContext = getTenantAwareBundleContextFromBundle(fromBundle, tenantPID);
        assertNotNull(fromContext);

        Bundle toBundle = getBundleBySymbolicName(fromContext, toBSN);
        assertNotNull(toBundle);

        File toRoot = toBundle.getBundleContext().getDataFile("");
        assertNull(toRoot);
    }

    /**
     * Test that the BundleContext is available for BundleEvents that are wrapped in a TenantAwareBundleEvent
     * by the TenantAwareBundleContext
     * <br/><br/>
     * This test relies on a multi-tenant bundle that publish the TenantAwareBundleContextService
     * service with different TENANT binding and BSN "org.amdatu.multitenant.itest.datastoretest2".
     * 
     * @throws Exception
     * @see AMDATUMT-9
     */
    public void testBundleFromTenantAwareBundleEventHasBundleContext() throws Exception {
        String tenantPID = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        Bundle dummyBundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.dummy");
        BundleStateChangeListener listener = new BundleStateChangeListener(dummyBundle, Bundle.ACTIVE);
        m_bundleContext.addBundleListener(listener);

        // make sure the dummy bundle is started before starting the actual test
        if (dummyBundle.getState() != Bundle.ACTIVE) {
            dummyBundle.start();
            listener.awaitState();
        }

        // BundleContext is not available when the bundle is STOPPED
        Bundle bundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.datastoretest2");
        BundleContext tenantAwareBundleContext = getTenantAwareBundleContextFromBundle(bundle, tenantPID);

        BundleStateChangeListener tenantAwareBundleStateListener =
            new BundleStateChangeListener(dummyBundle, Bundle.RESOLVED);
        tenantAwareBundleContext.addBundleListener(tenantAwareBundleStateListener);

        dummyBundle.stop();

        Bundle bundleFromEvent = tenantAwareBundleStateListener.awaitState();
        assertNull("Expect the BundleContext to be null when the bundle is Bundle is stopped",
            bundleFromEvent.getBundleContext());

        // BundleContext is available when the bundle is ACTIVE
        Bundle bundle1 = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.datastoretest2");
        BundleContext tenantAwareBundleContext1 = getTenantAwareBundleContextFromBundle(bundle1, tenantPID);

        BundleStateChangeListener tenantAwareBundleStateListener1 =
            new BundleStateChangeListener(dummyBundle, Bundle.ACTIVE);
        tenantAwareBundleContext1.addBundleListener(tenantAwareBundleStateListener1);

        dummyBundle.start();

        Bundle bundleFromEvent1 = tenantAwareBundleStateListener1.awaitState();
        assertNotNull("Expect the BundleContext not to be null when the bundle is Bundle is active",
            bundleFromEvent1.getBundleContext());
    }

    private class BundleStateChangeListener implements BundleListener {

        private int m_targetState;

        private CountDownLatch m_latch;

        private Bundle m_bundle;

        private Bundle m_bundleFromEvent;

        public BundleStateChangeListener(Bundle bundle, int targetState) {
            m_bundle = bundle;
            m_targetState = targetState;
            m_latch = new CountDownLatch(1);
        }

        @Override
        public void bundleChanged(BundleEvent event) {
            if (event.getBundle() != null && event.getBundle().equals(m_bundle)
                && event.getBundle().getState() == m_targetState) {
                m_bundleFromEvent = event.getBundle();
                m_latch.countDown();
            }
        }

        public Bundle awaitState() throws InterruptedException {
            if (m_latch.await(2l, TimeUnit.SECONDS)) {
                return m_bundleFromEvent;
            }
            else {
                throw new IllegalStateException("Bundle never reached target state " + m_targetState);
            }
        }
    }

    /**
     * Test that the tenant aware data storage area of a component is:
     * 
     * <ul>
     * <li> created when a tenant was added while the component was inactive</li>
     * <li> retains it's contents when an existing tenant is reconfigured </li>
     * <li> removed when a tenant is removed while the component is active</li>
     * <li> created when a tenant is added while the component is active</li>
     * <li> removed when a tenant was removed while the component was inactive</li>
     * </ul>
     * 
     * <br/><br/>
     * This test relies on a multi-tenant bundle that publish the TenantAwareBundleContextService
     * service with different TENANT binding and BSN "org.amdatu.multitenant.itest.datastoretest2".
     * 
     * @throws Exception
     * @see AMDATUMT-12
     */
    public void testTenantAwareBundleDataStoreLifecycle() throws Exception {

        String tenantPID = generateTenantPID();

        Bundle bundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.datastoretest2");
        assertNotNull("Expect the bundle under tests to be available", bundle);

        File dataFile = bundle.getBundleContext().getDataFile(tenantPID);
        assertFalse("Expect tenant aware data store doesn't exist before tenants are configured", dataFile.exists());

        bundle.stop();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);
        assertFalse("Expect tenant aware data store doesn't exist before bundle is started", dataFile.exists());

        bundle.start();
        assertTrue("Expect tenant aware data store exists after bundle is started", dataFile.exists());

        // Create file and dir in the tenant aware data store to validate data store contents
        // remain intact when reconfiguring a tenant
        new File(dataFile, "test").createNewFile();
        new File(dataFile, "testDir").mkdir();

        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM),
            new TenantWithProperties("Default"), new TenantWithProperties(tenantPID, NAME_KEY, "test"));

        assertTrue("Expect tenant aware data store exists after tenants are reconfigured", dataFile.exists());
        assertTrue("Expect contents of the tenant aware datastoure not to be removed after tenants are reconfigured",
            new File(dataFile, "test").exists());
        assertTrue("Expect contents of the tenant aware datastoure not to be removed after tenants are reconfigured",
            new File(dataFile, "testDir").exists());

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertFalse("Expect datafile doesn't exist after tenant was removed", dataFile.exists());

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);
        assertTrue("Expect tenant aware data store exists after tenant was added", dataFile.exists());

        bundle.stop();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        bundle.start();
        assertFalse("Expect datafile doesn't exist after was tenant removed and bundle is started", dataFile.exists());

    }

    /**
     * Test that the tenant binding scope can be narrowed using the X-MultiTenant-BindingFilter header.
     * 
     * This test relies on 3 multi-tenant bundles that publish the TenantAwareBundleContextService
     * service with different scopes and <pre>X-MultiTenant-BindingFilter: (optionalBundle=true)</pre>
     * <table>
     * <thead>
     * <tr>
     * <td>BSN</td>
     * <td>Binding</td>
     * </tr>
     * </thead>
     * <tbody>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest1</td>
     * <td>PLATFORM</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest2</td>
     * <td>TENANT</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest3</td>
     * <td>BOTH</td>
     * </tr>
     * </tbody>
     * </table>
     * 
     * @see AMDATUMT-8
     * @throws Exception
     */
    public void testBindingFilter() throws Exception {
        String tenantPID1 = generateTenantPID();
        String tenantPID2 = generateTenantPID();

        // Only tenant1 has the optional bundle enabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID1, "optionalBundle", true), new TenantWithProperties(tenantPID2,
            "optionalBundle", false));

        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest1",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest2", tenantPID1);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest2",
            Constants.PID_VALUE_PLATFORM, tenantPID2);

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest3", tenantPID1);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest3",
            Constants.PID_VALUE_PLATFORM, tenantPID2);

        // Only the PLATFORM tenant has the optional bundle enabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            true), new TenantWithProperties(tenantPID1, "optionalBundle", false), new TenantWithProperties(tenantPID2,
            "optionalBundle", false));

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest1",
            Constants.PID_VALUE_PLATFORM);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest1", tenantPID1,
            tenantPID2);

        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest2",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest3",
            Constants.PID_VALUE_PLATFORM);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest3", tenantPID1,
            tenantPID2);

        // all tenants have the optional bundle disabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM),
            new TenantWithProperties(tenantPID1, "optionalBundle", false), new TenantWithProperties(tenantPID2,
                "optionalBundle", false));

        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest1",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);

        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest2",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);

        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest3",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);

        // all tenants have the optional bundle enabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            true), new TenantWithProperties(tenantPID1, "optionalBundle", true), new TenantWithProperties(tenantPID2,
            "optionalBundle", true));
        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest1",
            Constants.PID_VALUE_PLATFORM);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest1", tenantPID1,
            tenantPID2);

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest2", tenantPID1,
            tenantPID2);
        assertTenantAwareBundleContextNotAvailable("org.amdatu.multitenant.itest.bindingfiltertest2",
            Constants.PID_VALUE_PLATFORM);

        assertTenantAwareBundleContextAvailable("org.amdatu.multitenant.itest.bindingfiltertest3",
            Constants.PID_VALUE_PLATFORM, tenantPID1, tenantPID2);
    }

    private void assertTenantAwareBundleContextAvailable(String BSN, String... tenantPIDs) throws Exception {
        Bundle bundle = getBundleBySymbolicName(m_bundleContext, BSN);
        for (String PID : tenantPIDs) {
            assertNotNull(getTenantAwareBundleContextFromBundle(bundle, PID));
        }
    }

    private void assertTenantAwareBundleContextNotAvailable(String BSN, String... tenantPIDs) throws Exception {
        Bundle bundle = getBundleBySymbolicName(m_bundleContext, BSN);
        for (String PID : tenantPIDs) {
            assertNull(getTenantAwareBundleContextFromBundle(bundle, PID));
        }
    }

    private Bundle getBundleBySymbolicName(BundleContext context, String name) {

        for (Bundle bundle : context.getBundles()) {
            if (bundle.getSymbolicName().equals(name)) {
                return bundle;
            }
        }
        return null;
    }

    /**
     * Test that the tenant aware data storage area of a component is
     * 
     * <ul>
     * <li>Not created for tenants that are not bound to a bundle </li>
     * <li>Removed for tenants that not bound to a bundle anymore</li>
     * </ul>
     * 
     * @throws Exception
     */
    public void testTenantAwareBundleDataStoreLifecycleWithBindingFilter() throws Exception {
        String tenantPID1 = generateTenantPID();
        String tenantPID2 = generateTenantPID();

        Bundle bundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.bindingfiltertest2");
        assertNotNull("Expect the bundle under tests to be available", bundle);

        bundle.stop();
        // Only tenant1 has the optional bundle enabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID1, "optionalBundle", true), new TenantWithProperties(tenantPID2,
            "optionalBundle", false));

        bundle.start();
        assertBundleDataStoreServiceAvailable(bundle, tenantPID1);
        assertBundleDataStoreServiceNotAvailable(bundle, tenantPID2);

        assertTenantAwareDataStoreExists(bundle, tenantPID1);
        assertTenantAwareDataStoreDoesNotExist(bundle, tenantPID2);

        // Only tenant2 has the optional bundle enabled
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID1, "optionalBundle", false), new TenantWithProperties(tenantPID2,
            "optionalBundle", true));

        assertBundleDataStoreServiceNotAvailable(bundle, tenantPID1);
        assertBundleDataStoreServiceAvailable(bundle, tenantPID2);

        // Expect the data store for tenant 1 to be removed as the tenant is no longer bound.
        assertTenantAwareDataStoreDoesNotExist(bundle, tenantPID1);
        assertTenantAwareDataStoreExists(bundle, tenantPID2);
    }

    private void assertTenantAwareDataStoreExists(Bundle bundle, String tenantPID) {
        File dataFile = bundle.getBundleContext().getDataFile(tenantPID);
        assertTrue(dataFile.exists());
    }

    private void assertTenantAwareDataStoreDoesNotExist(Bundle bundle, String tenantPID) {
        File dataFile = bundle.getBundleContext().getDataFile(tenantPID);
        assertFalse(dataFile.exists());
    }

    private void assertBundleDataStoreServiceAvailable(Bundle bundle, String tenantPID) {
        assertEquals(
            1,
            countServices(m_bundleContext, BundleDataStore.class.getName(),
                String.format("(&(%s=%s)(%s=%s))", PID_KEY, tenantPID, BUNDLE_ID, bundle.getBundleId())));
    }

    private void assertBundleDataStoreServiceNotAvailable(Bundle bundle, String tenantPID) {
        assertEquals(
            0,
            countServices(m_bundleContext, BundleDataStore.class.getName(),
                String.format("(&(%s=%s)(%s=%s))", PID_KEY, tenantPID, BUNDLE_ID, bundle.getBundleId())));
    }

    /**
     * 
     * 
     * <br/><br/>
     * This test relies on 3 multi-tenant bundles that publish the TenantAwareBundleContextService
     * service with different scopes.
     * <table>
     * <thead>
     * <tr>
     * <td>BSN</td>
     * <td>Binding</td>
     * </tr>
     * </thead>
     * <tbody>
     * </tr>
     * </thead>
     * <tbody>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest1</td>
     * <td>PLATFORM</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest2</td>
     * <td>TENANT</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bindingfiltertest3</td>
     * <td>BOTH</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bundlescopetest1</td>
     * <td>PLATFORM</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bundlescopetest2</td>
     * <td>TENANT</td>
     * </tr>
     * <tr>
     * <td>org.amdatu.multitenant.itest.bundlescopetest3</td>
     * <td>BOTH</td>
     * </tr>
     * </tbody>
     * </table>
     * 
     * @throws Exception
     * @see AMDATUMT-13
     */
    public void testBundleScope() throws Exception {

        String tenantPID = generateTenantPID();
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID, "optionalBundle", false));

        Bundle bundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.bundlescopetest2");
        assertNotNull(bundle);
        BundleContext bundleContext = getTenantAwareBundleContextFromBundle(bundle, tenantPID);
        assertNotNull(bundleContext);

        assertNull(getBundleBySymbolicName(bundleContext, "org.amdatu.multitenant.itest.bindingfiltertest2"));

        BundleTracker<Bundle> bundleTracker =
            new BundleTracker<Bundle>(bundleContext, Bundle.ACTIVE, new BundleTrackerCustomizer<Bundle>() {

                @Override
                public Bundle addingBundle(Bundle bundle, BundleEvent event) {
                    System.out.println("Added: " + bundle.getSymbolicName());
                    return bundle;
                }

                @Override
                public void modifiedBundle(Bundle bundle, BundleEvent event, Bundle object) {
                    System.out.println("Modified: " + bundle.getSymbolicName());
                }

                @Override
                public void removedBundle(Bundle bundle, BundleEvent event, Bundle object) {
                    System.out.println("Removed: " + bundle.getSymbolicName());
                }

            });

        bundleTracker.open();

        Thread.sleep(500l);
        System.out.println("= Reconfigure tenant ( enable optionalBundle )=");
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID, "optionalBundle", true));

        Thread.sleep(500l);
        System.out.println("= Reconfigure tenant ( disable optionalBundle )=");
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM, "optionalBundle",
            false), new TenantWithProperties(tenantPID, "optionalBundle", false));

        Thread.sleep(1500l);
        System.out.println("\n\nCLOSE=====");

        bundleTracker.close();

    }

    /**
     * Test using a {@link BundleTracker} with a {@link TenantAwareBundleContext}
     * 
     * This test relies on a multi-tenant bundle that publishes the TenantAwareBundleContextService with BSN "org.amdatu.multitenant.itest.datastoretest2" and
     * a multi-tenant bundle with BSN "org.amdatu.multitenant.itest.dummy"
     * 
     * @see AMDATUMT-4
     * @throws Exception
     */
    public void testUsingBundleTrackerWithTenantAwareBundleContext() throws Exception {
        String tenantPID = generateTenantPID();

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, tenantPID);

        Bundle bundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.datastoretest2");
        assertNotNull("Expect the bundle under tests to be available", bundle);
        Bundle dummyBundle = getBundleBySymbolicName(m_bundleContext, "org.amdatu.multitenant.itest.dummy");
        assertNotNull("Expect the bundle under tests to be available", dummyBundle);

        BundleContext bundleContext = getTenantAwareBundleContextFromBundle(bundle, tenantPID);

        BundleTrackerCustomizerImplementation customizer = new BundleTrackerCustomizerImplementation(dummyBundle);
        BundleTracker<Bundle> tracker = new BundleTracker<Bundle>(bundleContext, Bundle.ACTIVE, customizer);

        tracker.open();
        customizer.assertBundleRemovedFromTrackerWhenStopped();
        customizer.assertBundleAddedToTrackerWhenStarted();
        tracker.close();
    }

    private final class BundleTrackerCustomizerImplementation implements BundleTrackerCustomizer<Bundle> {

        private CountDownLatch latch;
        private final Bundle dummyBundle;

        private BundleTrackerCustomizerImplementation(Bundle dummyBundle) {
            this.dummyBundle = dummyBundle;
        }

        public void assertBundleRemovedFromTrackerWhenStopped() throws Exception {
            latch = new CountDownLatch(1);
            dummyBundle.stop();
            if (!latch.await(1, TimeUnit.SECONDS)) {
                fail("Dummy bundle was not removed from tracker");
            }
        }

        public void assertBundleAddedToTrackerWhenStarted() throws Exception {
            latch = new CountDownLatch(1);
            dummyBundle.start();
            if (!latch.await(1, TimeUnit.SECONDS)) {
                fail("Dummy bundle was not added to tracker");
            }
        }

        @Override
        public Bundle addingBundle(Bundle bundle, BundleEvent event) {
            if (latch != null) {
                latch.countDown();
            }
            return bundle;
        }

        @Override
        public void modifiedBundle(Bundle bundle, BundleEvent event, Bundle object) {

        }

        @Override
        public void removedBundle(Bundle bundle, BundleEvent event, Bundle object) {
            latch.countDown();
        }
    }

    private BundleContext getTenantAwareBundleContextFromBundle(Bundle bundle, String pid) throws Exception {

        ServiceReference[] serviceRefs =
            m_bundleContext.getServiceReferences(TenantAwareBundleContextService.class.getName(),
                String.format("(%1$s=%2$s)", PID_KEY, pid));
        if (serviceRefs == null)
            return null;

        ServiceReference serviceRef = null;
        for (ServiceReference ref : serviceRefs) {
            if (ref.getBundle().getBundleId() == bundle.getBundleId()) {
                serviceRef = ref;
                break;
            }
        }
        if (serviceRef == null)
            return null;

        TenantAwareBundleContextService service =
            (TenantAwareBundleContextService) m_bundleContext.getService(serviceRef);
        if (service == null)
            return null;

        return service.getBundleContext();
    }

    private Properties createTenantConfiguration(String tenantPID) {
        Properties properties = new Properties();
        properties.put(PID_KEY, tenantPID);
        properties.put(NAME_KEY, "Tenant " + tenantPID);
        return properties;
    }

    private String generateTenantPID() {
        return String.format("mt%s", Long.toHexString(System.nanoTime()));
    }

    private int getConfigurationCount() {
        return m_configurations.size() + 1;
    }

    private void removeTenantConfig(Configuration config) throws Exception {
        m_configurations.remove(config);
        config.delete();
        m_tenantConfigurator.waitForSystemToSettle();
    }

    private Configuration addTenantConfig(Properties properties) throws Exception {
        Configuration config = m_tenantConfigurator.updateFactoryConfig("Default", properties);
        m_configurations.add(config);
        m_tenantConfigurator.waitForSystemToSettle();
        return config;
    }
}
