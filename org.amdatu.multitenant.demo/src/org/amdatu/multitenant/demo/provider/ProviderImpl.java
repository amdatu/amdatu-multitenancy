/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.demo.provider;

import org.amdatu.multitenant.Tenant;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Demo provider component that returns the tenant's configured {@code demo.message}
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ProviderImpl implements BundleActivator, Provider {

    private BundleContext m_context;
    private ServiceRegistration<?> m_registration;

    @Override
    public void start(BundleContext context) throws Exception {
        m_context = context;
        m_registration = context.registerService(Provider.class, this, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (m_registration != null) {
            m_registration.unregister();
        }
    }

    @Override
    public String getMessage() {
        String message = "UNKNOWN";
        try {
            ServiceReference<Tenant> tr = m_context.getServiceReference(Tenant.class);
            if (tr != null) {
                Tenant t = m_context.getService(tr);
                if (t != null) {
                    message = (String) t.getProperties().get("demo.message");
                    m_context.ungetService(tr);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }
}
